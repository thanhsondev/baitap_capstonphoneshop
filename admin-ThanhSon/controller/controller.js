

function renderDSSP (dsspArr) {
    var contentHTML = "";
    dsspArr.forEach(function (item) {
        var content = `<tr>
        <td>${item.id}</td>
        <td>${item.img}</td>
        <td>${item.name}</td>
        <td>${item.price}đ</td>
        <td>${item.screen}</td>
        <td>${item.blackCamera}</td>
        <td>${item.frontCamera}</td>
        <td>${item.desc}</td>
        <td>${item.type}</td>
        <td>
        <button onclick="xoaSP(${item.id})" class="bg-red-400 hover:bg-red-500 text-white font-bold py-2 px-4 rounded">
        Xóa
        </button>
        <button onclick="suaSP(${item.id})" class="bg-yellow-400 hover:bg-yellow-500 text-white font-bold py-2 px-4 rounded">
        Sửa
      </button>
        </td>
        </tr>`

        contentHTML += content;
    });
    document.getElementById("tbodySanPham").innerHTML = contentHTML;

}

function batLoading () {
    document.getElementById("spinner").style.display = "flex";
};

function tatLoading () {
    document.getElementById("spinner").style.display = "none";
}


function laySanPhamTuForm () {
  
  var ten =  document.getElementById("name").value;
  var gia=  document.getElementById("price").value ;
  var manHinh =  document.getElementById("screen").value  ;
  var blackCam =  document.getElementById("blackcam").value ;
  var frontCam =  document.getElementById("frontcam").value ;
  var img =  document.getElementById("img").value ;
  var moTa =  document.getElementById("desc").value  ;
  var loai =  document.getElementById("type").value  ;

    return {
        name: ten,
        price: gia,
        screen: manHinh,
        blackCamera: blackCam,
        frontCamera: frontCam,
        img: img,
        desc: moTa,
        type: loai,
    };

}

function showThongTinLenForm (product) {
    document.getElementById("name").value = product.name;
    document.getElementById("price").value = product.price;
    document.getElementById("screen").value = product.screen ;
    document.getElementById("blackcam").value = product.blackCamera;
    document.getElementById("frontcam").value = product.frontCamera;
    document.getElementById("img").value = product.img;
    document.getElementById("desc").value = product.desc ;
    document.getElementById("type").value = product.type ;
}