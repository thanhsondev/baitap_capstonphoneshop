


function fetchProductList () {
    batLoading();
    productServ
        .getList()
        .then((res) => {
                renderDSSP(res.data);
                tatLoading();

        }).catch((err) => {
                tatLoading();
        });
        document.getElementById("formInput").reset();
};

fetchProductList();

function themSP () {
        batLoading();
        var newSanPham = laySanPhamTuForm();
        productServ
        .create(newSanPham)
        .then(function(res) {
                tatLoading();
                fetchProductList();
                resetInput(res.data);
        }).catch(function(err) {
               tatLoading();
        });
        
}

function xoaSP (id) {
        batLoading();
        productServ
        .delete(id)
        .then(function(res) {
               fetchProductList();
               tatLoading();
        }).catch(function(err) {
              tatLoading();
        });
       
};


var idProductUpdate = null;

function suaSP(id) {
        idProductUpdate = id;
        batLoading();
        productServ
        .getbyID(id)
        .then((res) => {
              showThongTinLenForm(res.data);
              tatLoading();  
        }).catch((err) => {
               tatLoading();
        });
};


function capNhatSP () {
        batLoading();
        var product = laySanPhamTuForm();
        productServ
        .update(idProductUpdate, product)
        .then((res) => {
               fetchProductList();
               tatLoading();
        }).catch((err) => {
               tatLoading();
        });
}


function ascending () {
        batLoading()
        productServ
        .getList()
        .then((res) => {
                let valueSelect = document.getElementById("sort").value;
                res.data.sort((a, b) => {
                        if (valueSelect === "az") {
                                return a.price.localeCompare(b.price)
                        } else if (valueSelect==="za"){
                                return b.price.localeCompare(a.price)
                        } else {
                                return a.id - b.id
                        }
                })
                renderDSSP(res.data);
                tatLoading();
        }).catch((err) => {
                tatLoading();
        });
      
}

function searchProduct () {

        batLoading();
        productServ
            .getList()
            .then((res) => {

                let valueSearch = document.getElementById("search").value;
                let nameSearch = res.data.filter(value => {
                        return value.name.toUpperCase().includes(valueSearch.toUpperCase())
                })
                    renderDSSP(nameSearch);
                    tatLoading();
    
            }).catch((err) => {
                    tatLoading();
            });
      

}

