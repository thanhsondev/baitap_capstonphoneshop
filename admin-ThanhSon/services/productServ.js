

const BASE_URL = "https://648c203f8620b8bae7ec489b.mockapi.io/shop";

var productServ = {
    getList: () => {
       return axios({
            url: BASE_URL,
            method: "GET",
     });
    },
    create: (product) => {
        return axios({
            url: BASE_URL,
            method: "POST",
            data: product,
     });
    },
    delete: (id) => {
        return axios({
            url: `${BASE_URL}/${id}`,
            method: "DELETE",
     });
    },
    getbyID: (id) => {
        return axios({
            url: `${BASE_URL}/${id}`,
            method: "GET",
     });  
    },
    update: (id, product) => {
        return axios({
            url: `${BASE_URL}/${id}`,
            method: "PUT",
            data: product,
     });    
    }

}